#!/bin/bash

if [ ! -f /usr/bin/tesseract ]
then
  echo "Please install tesseract"
  echo "sudo apt install tesseract-ocr"
  exit 1
fi

rm *.png 2> /dev/null 
wget "https://storage.googleapis.com/gctf-2018-attachments/7ad5a7d71a7ac5f5056bb95dd326603e77a38f25a76a1fb7f7e6461e7d27b6a3" -qO ocr.zip
unzip -q ocr.zip

echo "One second please..."

convert OCR_is_cool.png -scale 200% -density 300 OCR_is_cool.png
TXT="$(tesseract OCR_is_cool.png - 2>/dev/null)"
for i in $(seq 25)
do
  echo $i $TXT |\
    tr $(printf %${i}s | tr ' ' '.')A-Za-z A-ZA-Za-z
done|tr '[:upper:]' '[:lower:]'| sed 's/ctf/\nCTF/g'|grep 'CTF{'|awk '{print $1}'|sed 's/pap/sas/g'


echo -e "\nhttps://www.youtube.com/watch?v=al1ZUsloHJA"
