#!/bin/bash

wget "https://storage.googleapis.com/gctf-2018-attachments/4e69382f661878c7da8f8b6b8bf73a20acd6f04ec253020100dfedbd5083bb39" -qO floppy.zip
unzip floppy.zip

binwalk -e foo.ico
clear
awk '/CTF/{print $1}' _foo.ico.extracted/driver.txt

#clean up
rm floppy.zip _foo* foo.ico -fr

echo -e "\nhttps://www.youtube.com/watch?v=KRbBzijAhhg"
