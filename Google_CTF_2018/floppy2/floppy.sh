#!/bin/bash

wget "https://storage.googleapis.com/gctf-2018-attachments/4e69382f661878c7da8f8b6b8bf73a20acd6f04ec253020100dfedbd5083bb39" -qO floppy.zip
unzip floppy.zip

binwalk -e foo.ico
mv _foo.ico.extracted/www.com .

clear
echo "Once in DOSbox press 'alt+pause'"
echo "Then use Page Down (in this window, not in the DOSbox windos)"
echo "until you see the flag"
echo "Press 'Enter' to continue..."
read

./dosbox-0.74/src/dosbox www.com

#clean up
rm -fr _foo.ico* www.com floppy.zip foo.ico

echo -e "\nhttps://www.youtube.com/watch?v=Z1EFcSZb_ss"
