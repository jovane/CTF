#!/bin/bash

echo "====Installing needed files===="
sudo apt install ncurses-dev make gcc

wget "http://http.debian.net/debian/pool/main/d/dosbox/dosbox_0.74.orig.tar.gz"
tar xvf dosbox_0.74.orig.tar.gz
rm dosbox_0.74.orig.tar.gz
cd dosbox-0.74
./autogen.sh
./configure --enable-debug=heavy
make

./src/dosbox

